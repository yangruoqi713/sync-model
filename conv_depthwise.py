import numpy as np
import tensorflow.compat.v1 as tf
tf.disable_v2_behavior()
import time
from tensorflow.python.framework import graph_util

# depthwise

in_batch = 1
in_h = 3
in_w = 3
in_c = 3
# input_data = np.random.uniform(low=-15,high=15,size=(in_batch, in_h, in_w, in_c)).astype(np.float32)
input_data = np.ones((in_batch* in_h* in_w* in_c)).astype(np.float32)
input_data.tofile("./convDw/convDwfp32_input.bin")
# input_data = np.fromfile("./convDw/convDwfp32_input.bin", dtype=np.float32)
input_data = input_data.reshape(in_batch, in_h, in_w, in_c)
print("input_data shape: ", input_data.shape)
print(input_data.flatten()[:100])
dtype = tf.float32
input = tf.placeholder(name="input", dtype=dtype, shape=(in_batch, in_h, in_w, in_c))

weight_h = 3
weight_w = 3
weight_ic = 3
weight_oc = 1
# weight_data = np.random.uniform(low=-100,high=100, size=(weight_h, weight_w, weight_ic, weight_oc)).astype(np.float32)
weight_data = np.arange((weight_h* weight_w* weight_ic* weight_oc)).astype(np.float32)
weight_data = weight_data.reshape(weight_h, weight_w, weight_ic, weight_oc)
weight_trans = weight_data.transpose(2, 3, 0, 1)
weight_trans.tofile("./convDw/convDwfp32_weight.bin")
# weight_data = np.fromfile("./convDw/convDwfp32_weight.bin", dtype=np.float32)
# weight_data = weight_data.reshape(weight_ic, weight_oc, weight_h, weight_w)
# weight_data = weight_data.transpose(2, 3, 0, 1)
print("weight_data shape: ", weight_data.shape)
print(weight_data.flatten()[:100])

print("weight_trans shape: ", weight_trans.shape)
print(weight_trans.flatten()[:100])

out = tf.nn.depthwise_conv2d(input, weight_data, strides=[1, 2, 2, 1],dilations=[1,1], padding="SAME")
output = tf.identity(out, name="output")


pb_file_name = "./convDw/convdwfp32.pb"
with tf.Session() as sess:
    sess.run(tf.global_variables_initializer())
    constant_graph = graph_util.convert_variables_to_constants(sess, sess.graph_def, ['output'])
    with tf.gfile.FastGFile(pb_file_name, mode="wb") as f:
        f.write(constant_graph.SerializeToString())
    op_tensor = sess.graph.get_tensor_by_name('output:0')
    op_result = sess.run(op_tensor, feed_dict={'input:0': input_data})
    print("==========output data:")
    print("output shape: ", op_result.shape)
    print("output type: ", type(op_result))
    print(op_result.flatten()[:200])
    # print(op_result.flatten()[21])
    op_result.tofile("./convDw/convDwfp32_output.bin")

    # with open(out_file, "w") as text_file:
    #     for name, v in net.blobs.items():
    #         data_flatten = np.squeeze(v.data).flatten()
    #         print(name)
    #         # print(data_flatten[:100])
    #
    #     for name, data in out.items():
    #         data_flatten = np.squeeze(data).flatten()
    #         # data_flatten = np.reshape(data_flatten, (batch, channel, 3, 3)) # nchw out
    #         # data_flatten_trans = data_flatten.transpose(0, 2, 3, 1)
    #         title = str(name) + ' ' + str(len(data.shape)) + ' '
    #         for i in range(len(data.shape)):
    #             title += str(data.shape[i]) + ' '
    #         text_file.write(title + '\n')
    #         out_size = data_flatten.shape[0]
    #         for k in range(out_size):
    #             text_file.write(str(data_flatten[k]) + ' ')
    #
    #         print(data_flatten[:243])
    #         # data_flatten.tofile(out_file)
    #
    # text_file.close()
