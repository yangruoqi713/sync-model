import numpy as np
import pytest

import mindspore as ms
import mindspore.nn as nn
import mindspore.ops as ops
from mindspore.ops import operations as P


class ReverseV2(nn.Cell):
    def __init__(self):
        super(ReverseV2, self).__init__()
        self.net = P.ReverseV2(axis=(0, ))

    def construct(self, x):
        return self.net(x)


np.set_printoptions(precision=6, suppress=True)

##### forward #####
# input_array = np.array([[1, 2, 3, 4], [5, 6, 7, 8]]).astype(np.float32)
# input_array = np.arange(4*3*2).reshape(4, 3, 2).astype(np.float32)
# # input_array = np.random.uniform(low=0, high=2, size=(2, 3)).astype(np.float32)
# print("input_array", input_array)
# x = ms.Tensor(input_array)

# net = ReverseV2()
# output = net(x)
# print("output", output)


##### backward #####
# grad_net = ops.grad(net)
# grad = grad_net(x)
# print(grad)


##### vmap #####
def cal_reverse_v2(x):
    return P.ReverseV2(axis=(0, ))(x)

x = ms.Tensor(np.arange(5*4*3*2).reshape(2, 3, 4, 5).astype(np.float32))
print("input", x.asnumpy())

net_vmap = ops.vmap(ops.vmap(cal_reverse_v2, in_axes=-1, out_axes=-1), in_axes=-1, out_axes=-1)
vmap_out = net_vmap(x).asnumpy()
print("vmap_out", vmap_out)


##### loop #####
out = []
for i in range(x.shape[-1]):
    inner_out = []
    for j in range(x.shape[-2]):
        out_t = cal_reverse_v2(x[:, :, j, i])
        inner_out.append(out_t.asnumpy())
    out.append(np.stack(inner_out))
loop_out = np.stack(out)
loop_out = loop_out.transpose(2, 3, 1, 0)
print("loop_out", loop_out)

assert np.allclose(vmap_out, loop_out)
