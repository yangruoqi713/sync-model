./benchmark --modelFile=mobilenet_v2_1.0_224.ms --inDataFile=mobilenet_v2_1.0_224.tflite.ms.bin --benchmarkDataFile=mobilenet_v2_1.0_224.tflite.ms.out --device=NPU --accuracyThreshold=1.5

./benchmark --modelFile=squeezenet.ms --inDataFile=squeezenet.tflite.ms.bin --benchmarkDataFile=squeezenet.tflite.ms.out --device=NPU --accuracyThreshold=1.5

./benchmark --modelFile=inception_v3.ms --inDataFile=inception_v3.tflite.ms.bin --benchmarkDataFile=inception_v3.tflite.ms.out --device=NPU


./benchmark --modelFile=mobilenet_v2_1.0_224.ms --device=NPU  --numThreads=2

./benchmark --modelFile=squeezenet.ms --device=NPU  --numThreads=2

./benchmark --modelFile=inception_v3.ms --device=NPU  --numThreads=2




./benchmark --modelFile=mobilenet_v2_1.0_224.ms --device=CPU  --enableFp16=true --numThreads=1 

./benchmark --modelFile=mobilenet_v2_1.0_224.ms --device=CPU  --enableFp16=true --numThreads=2

./benchmark --modelFile=mobilenet_v2_1.0_224.ms --device=CPU  --enableFp16=true --numThreads=4



./benchmark --modelFile=squeezenet.ms --device=CPU  --enableFp16=true --numThreads=1 

./benchmark --modelFile=squeezenet.ms --device=CPU  --enableFp16=true --numThreads=2

./benchmark --modelFile=squeezenet.ms --device=CPU  --enableFp16=true --numThreads=4



./benchmark --modelFile=inception_v3.ms --device=CPU  --enableFp16=true --numThreads=1 

./benchmark --modelFile=inception_v3.ms --device=CPU  --enableFp16=true --numThreads=2

./benchmark --modelFile=inception_v3.ms --device=CPU  --enableFp16=true --numThreads=4



./benchmark --modelFile=mobilenet_v2_1.0_224.ms --device=NPU  --numThreads=1 

./benchmark --modelFile=mobilenet_v2_1.0_224.ms --device=NPU  --numThreads=2

./benchmark --modelFile=mobilenet_v2_1.0_224.ms --device=NPU  --numThreads=4



./benchmark --modelFile=squeezenet.ms --device=NPU  --numThreads=1 

./benchmark --modelFile=squeezenet.ms --device=NPU  --numThreads=2

./benchmark --modelFile=squeezenet.ms --device=NPU  --numThreads=4



./benchmark --modelFile=inception_v3.ms --device=NPU  --numThreads=1 

./benchmark --modelFile=inception_v3.ms --device=NPU  --numThreads=2

./benchmark --modelFile=inception_v3.ms --device=NPU  --numThreads=4


