#!/bin/bash

./benchmark --modelFile=mobilenet_v2_1.0_224.ms --device=CPU  --numThreads=1
./benchmark --modelFile=mobilenet_v2_1.0_224.ms --device=CPU  --numThreads=2
./benchmark --modelFile=mobilenet_v2_1.0_224.ms --device=CPU  --numThreads=4


./benchmark --modelFile=squeezenet.ms --device=CPU  --numThreads=1
./benchmark --modelFile=squeezenet.ms --device=CPU  --numThreads=2
./benchmark --modelFile=squeezenet.ms --device=CPU  --numThreads=4


./benchmark --modelFile=inception_v3.ms --device=CPU  --numThreads=1
./benchmark --modelFile=inception_v3.ms --device=CPU  --numThreads=2
./benchmark --modelFile=inception_v3.ms --device=CPU  --numThreads=4



./benchmark --modelFile=mobilenet_v2_1.0_224.ms --device=NPU  --numThreads=1
./benchmark --modelFile=mobilenet_v2_1.0_224.ms --device=NPU  --numThreads=2
./benchmark --modelFile=mobilenet_v2_1.0_224.ms --device=NPU  --numThreads=4


./benchmark --modelFile=squeezenet.ms --device=NPU  --numThreads=1
./benchmark --modelFile=squeezenet.ms --device=NPU  --numThreads=2
./benchmark --modelFile=squeezenet.ms --device=NPU  --numThreads=4


./benchmark --modelFile=inception_v3.ms --device=NPU  --numThreads=1
./benchmark --modelFile=inception_v3.ms --device=NPU  --numThreads=2
./benchmark --modelFile=inception_v3.ms --device=NPU  --numThreads=4



