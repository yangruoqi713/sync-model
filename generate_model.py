import numpy as np
import tensorflow.compat.v1 as tf
tf.disable_v2_behavior()
from tensorflow.python.framework import graph_util

if __name__ == "__main__":
    pb_file = "conv.pb"
    tf_input1 = tf.placeholder(name="input", dtype="float32", shape=[1, 224, 224, 3])
    # kernel1 = np.arange(4*3*2*2).astype(np.float32)
    # kernel1 = kernel1.reshape(2, 2, 3, 4) # kh, kw, ic, oc

    # kernel1 = tf.Variable(tf.random_normal([3, 3, 3, 32]))
    # kernel1 = tf.Variable(tf.random_normal([3, 3, 3, 32]))
    # weight_file = "/home/yangruoqi/workspace/project/tmp/mindspore/mindspore/lite/build/weight_nchw.bin"
    # weight_data =  np.fromfile(weight_file, np.float32)
    weight_data = np.random.uniform(low=0, high=1, size=(3* 3* 3* 32)).astype(np.float32)
    weight_data = weight_data.reshape([32, 3, 3, 3])
    kernel1 = weight_data.transpose(2, 3, 1, 0)
    conv = tf.nn.conv2d(tf_input1, kernel1, strides=[1,2,2,1], padding="SAME", name="conv")
    # relu = tf.nn.relu6(conv, name="relu")

    # kernel2 = tf.Variable(tf.random_normal([3, 3, 32, 3]))
    # conv2 = tf.nn.conv2d(relu, kernel2, strides=[1,1,1,1], padding="SAME", name="conv2")

    with tf.Session() as sess:
        sess.run(tf.global_variables_initializer())
        constant_graph = graph_util.convert_variables_to_constants(sess, sess.graph_def, ['conv'])
        with tf.gfile.FastGFile(pb_file, mode="wb") as f:
            f.write(constant_graph.SerializeToString())