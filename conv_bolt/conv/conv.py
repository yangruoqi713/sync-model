import numpy as np
import torch.nn as nn
import torch.nn.functional as F
import torch

device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')


class Net(nn.Module):
    def __init__(self):
        super(Net, self).__init__()
        self.conv1 = nn.Conv2d(in_channels=1, out_channels=3, kernel_size=3)
        self.conv2 = nn.Conv2d(in_channels=3, out_channels=2, kernel_size=3)
        self.mp = nn.MaxPool2d(2, 2)
        self.mp1 = nn.MaxPool2d(2, 2)
        self.fc1 = nn.Linear(400, 120)
        self.fc2 = nn.Linear(120, 84)

        torch.arange(1 * 3 * 9, out=self.conv1.weight.data)
        torch.arange(2 * 3 * 9, out=self.conv2.weight.data)
        self.conv2.weight.data = self.conv2.weight.data * -1
        torch.zero_(self.conv1.bias.data)

    def forward(self, x):
        conv_1 = F.relu(self.conv1(x))
        # conv_2 = F.relu(self.conv2(conv_1))
        return conv_1


net = Net()
model = net.to(device)

# torch.save(model, './model_para.pth')
torch.save(model, './model_para.pth')

N = 1
C = 1
H = 32
W = 32

torch_model = torch.load("./model_para.pth")  # pytorch模型加载
batch_size = 1  # 批处理大小
input_shape = (N, C, H, W)  # 输入数据

# set the model to inference mode
torch_model.eval()

in_data = np.arange(N * C * H * W).astype(np.float32) * 0.1
in_data = in_data.reshape(N, C, H, W)
in_data.tofile("conv_nchw.bin")
nhwc_data = in_data.transpose(0, 2, 3, 1)
nhwc_data.tofile("conv_nhwc.bin")
x = torch.tensor(in_data)  # 生成张量

export_onnx_file = "conv.onnx"  # 目的ONNX文件名
torch.onnx.export(torch_model,
                  x,
                  export_onnx_file,
                  opset_version=10,
                  do_constant_folding=True,  # 是否执行常量折叠优化
                  input_names=["input"],  # 输入名
                  output_names=["output"],  # 输出名
                  # dynamic_axes={"input": {0: "batch_size"},  # 批处理变量
                  #               "output": {0: "batch_size"}}
                  )

output = net(x)
print(output)
output.detach().numpy().tofile("conv_out.bin")

# torch.save(model.state_dict(), './model_para.pth')
